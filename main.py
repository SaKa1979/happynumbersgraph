import sys
import networkx as nx
import json
from networkx.readwrite import json_graph
import http_server as server

sys.setrecursionlimit(10000)

'''
Takes each digit from the supplied integer and enters them
into the returned list.
'''


def int_to_list(n):
    res = list(map(int, str(n)))
    return res


'''
Takes a supplied list of digits and returns these digits as
a integer.
'''


def list_to_int(ns):
    res = int(''.join(map(str, ns)))
    return res


'''
Test whether the supplied integer is a Happy number.
Returns the tuple (bool,tree) of is-happy and the sequence of numbers.
'''


def is_happy(n, l=[]):
    l.append(n)
    # End of tree
    if n == 1:
        return True, l
    nr_as_digits = int_to_list(n)
    nr_squared = sum(map(lambda x: x ** 2, nr_as_digits))
    # We are looping!
    if nr_squared in l:
        # Add recurring number to get a full circle
        l.append(nr_squared)
        return False, l
    # Recursive step
    return is_happy(nr_squared, l)


def generate_graph(tree_in=[]):
    G = nx.DiGraph()

    # Add the entire sequence to the tree
    for i in range(0, len(tree) - 1):
        G.add_edge(tree[i], tree[i + 1])

    for n in G:
        G.nodes[n]['name'] = n

    d = json_graph.node_link_data(G)
    json.dump(d, open('force/force.json', 'w'))
    server.load_url('force/force.html')


'''
Main()
'''
input_nr = int(input("Enter number:"))
happy, tree = is_happy(input_nr);
generate_graph(tree)
