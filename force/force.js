var svg = d3.select("svg"),
    width = +svg.attr("width"),
    height = +svg.attr("height");

var simulation = d3.forceSimulation()
    .force("link", d3.forceLink().id(function (d) {
        return d.id;
    }))
    .force("charge", d3.forceManyBody())
    .force("center", d3.forceCenter(width / 2, height / 2));

d3.json("force.json", function (error, graph) {
    if (error) throw error;

    // Create link array and join it to the json link content
    var link = svg.append("g")
        .attr("class", "links")
        .selectAll("line")
        .data(graph.links)
        .enter().append("line");

    // Create node array and join it to the json node content
    var node = svg.append("g")
        .attr("class", "nodes")
        .selectAll("circle")  // selects all circles (whether they exist or not)
        .data(graph.nodes) // joins the data to the current selection (all circles)
        .enter().append("circle") // joins the data to placeholders for the circles
        .call(d3.drag() // link to mouse action functions
            .on("start", dragstarted)
            .on("drag", dragged)
            .on("end", dragended));

    // Create label array and join it to the json node content
    var nodeLabel = svg.append("g")
        .selectAll("label")
        .data(graph.nodes)
        .enter().append("text")
        .call(d3.drag()
            .on("start", dragstarted)
            .on("drag", dragged)
            .on("end", dragended));

    // Manipulate node attributes
    var nodeAttr = node
        .attr("r", function(d) {
            return calc_size(d.id);
        })
        .style("fill", "yellow");

    // Manipulate label attributes
    var nodeText = nodeLabel
        .style("font-size", "10")
        .text(function(d){return d.name;});

    // Various methods required for dynamic behaviour.
    simulation
        .nodes(graph.nodes)
        .on("tick", ticked);

    simulation.force("link")
        .links(graph.links);

    function calc_size(number) {
        digits = Math.floor( Math.log10( number ) ) + 1;
        size = 10 + (0.9 * digits) - (0.03 * digits);
        return size;
    }

    function ticked() {
        link
            .attr("x1", function (d) {
                return d.source.x;
            })
            .attr("y1", function (d) {
                return d.source.y;
            })
            .attr("x2", function (d) {
                return d.target.x;
            })
            .attr("y2", function (d) {
                return d.target.y;
            });

        node
            .attr("cx", function (d) {
                return d.x;
            })
            .attr("cy", function (d) {
                return d.y;
            });

        nodeLabel
            .attr("dx", function (d) {
                return d.x - 5 ;
            })
            .attr("dy", function (d) {
                return d.y;
            });
    }
});

function dragstarted(d) {
    if (!d3.event.active) simulation.alphaTarget(0.1).restart();
    d.fx = d.x;
    d.fy = d.y;
}

function dragged(d) {
    d.fx = d3.event.x;
    d.fy = d3.event.y;
}

function dragended(d) {
    if (!d3.event.active) simulation.alphaTarget(0);
    d.fx = null;
    d.fy = null;
}